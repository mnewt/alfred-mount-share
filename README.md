# Alfred Mount Share

Workflow to mount and unmount SMB shares using [Alfred](https://www.alfredapp.com/)

## Requirements

 - [Alfred Power Pack](https://www.alfredapp.com/powerpack/)

## Installation

1. Download [Mount Share.alfredworkflow](https://gitlab.com/mnewt/alfred-mount-share/raw/master/Mount%20Share.alfredworkflow)
2. Double click it to load it in Alfred
3. Alfred Settings -> Workflows -> Mount Share -> Click the ![[x]](x.png) in the upper right corner
4. Click the **+** and add these **Workflow Environment Variables**

| Name     | Value                          |
| -------- | ------------------------------ |
| server   | `your-default-server-hostname` |
| username | `your-username`                |
| password | `your-password`                |

## Usage

### Mount a share

Type `m`, then space to look up and choose the server configured in the **Workflow Environment Variables**. Choose a share to mount it.

`m`

Or specify a different server:

`m some-server-hostname`

### Unmount a share

Type `u`, then space to look up the currently mounted shares. Choose one to unmount it

`u `

## Contributing

1. Fork it
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request

## History

8/27/2016: 1.0

## License

[MIT](LICENSE)
